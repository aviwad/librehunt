# LibreHunt JS

My MYP personal project, a website to aid you on your libre/distro hunt, now in client-side JavaScript

# Use

Due to the convenience of GitHub pages, I moved LibreHunt to GitHub! You can find it there:
<link>https://www.github.com/aviwad/librehunt</link>

This repo is archived.

# License

**GPL 3**

This file is part of LibreHunt JS.

LibreHunt JS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LibreHunt JS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LibreHunt JS.  If not, see <https://www.gnu.org/licenses/>.

Copyright 2018, Avi Wadhwa

